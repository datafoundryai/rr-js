/* eslint-disable linebreak-style */
const logger = require("./lib/nodeBunyan")({name: "test-service", logLevel: {stdout: "debug", fileStream: "debug", dpaStream: "debug"}});

logger.info("f59398ed-c03e-4ad6-8199-e3c2f988fb06", "scheduler-service", "info", {"test": "test"});
logger.debug("f59398ed-c03e-4ad6-8199-e3c2f988fb06", "scheduler-service", "debug", {"test": "test"});
logger.trace("f59398ed-c03e-4ad6-8199-e3c2f988fb06", "scheduler-service", "trace", {"test": "test"});
