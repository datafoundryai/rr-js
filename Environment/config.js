// Get the app name from package.json which presents in root folder of the main repo

let pJson;
try {
  pJson = require("../../../package.json");
} catch(e) {
  console.log(e);
}

// apiPath: process.env.REACT_APP_APIURL || "https://api.df-dev.net",
exports.loggerEnv = {
  filePathDir: "/logs",
  period: "1d", // daily rotation
  totalFiles: 5, // keep 5 back copies
  threshold: "10m", // Rotate log files larger than 10 megabytes
  totalSize: "50m", // Don't keep more than 50mb of archived log files
  gzip: true,
  apiPath: process.env.REACT_APP_APIURL,
  appName: pJson ? pJson.name : null,
  level: process.env.LEVEL || "info"
};
