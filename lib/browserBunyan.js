import { createLogger } from "browser-bunyan";
import { ServerStream } from "@browser-bunyan/server-stream";
const { loggerEnv } = require("../Environment/config");
// eslint-disable-next-line max-len
import { ConsoleFormattedStream } from "@browser-bunyan/console-formatted-stream";

const browserBunyan = ({ name, logLevel }) => {
  const logger = createLogger({
    name: name || loggerEnv.appName,
    streams: [
      {
        level: logLevel && logLevel["stdout"] ? logLevel["stdout"] : loggerEnv.level,
        stream: new ConsoleFormattedStream(),
      },
      {
        level: logLevel && logLevel["dpaStream"] ? logLevel["dpaStream"] : loggerEnv.level,
        stream: new ServerStream({
          url: `${loggerEnv.apiPath}/dpaService/RR/init`,
          method: "POST",
          throttleInterval: 0,
          flushOnClose: true,
        }),
      },
    ],
  });

  const log_levels = ["fatal", "error", "warn", "info", "debug", "trace"];

  const logLevelWrappers = {};
  log_levels.forEach((level) => {
    logLevelWrappers[level] = (rrId, source, message, optionalParam) => {
      logger[level]({
        rrId, source, message, ...optionalParam, logLevel: level, app: loggerEnv.appName,
        timestamp: new Date()
      });
    }
  });
  return logLevelWrappers;
}

export default browserBunyan;