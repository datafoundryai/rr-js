const axios = require("axios").default;
const { loggerEnv } = require("../Environment/config");
const {isURL} = require("validator");

// validate the url once here and if it is valid the send logs to server
let isValidUrl;
try {
  isValidUrl = isURL(loggerEnv.apiPath);
} catch(e) {
  console.log("Invalid apiPath, so can't send logs to DPA server");
}

function DPAStream() {}
DPAStream.prototype.write = function (rec) {
    const parsedRec = JSON.parse(rec);

    if (isValidUrl) {
      axios({
        url: `${loggerEnv.apiPath}/dpaService/RR/init`,
        method: "POST",
        data: parsedRec
      }).catch((e) => console.log(e));
    }
}

module.exports = DPAStream;
