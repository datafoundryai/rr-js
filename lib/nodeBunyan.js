/* eslint-disable linebreak-style */
const bunyan = require("bunyan");
const RotatingFileStream = require("bunyan-rotating-file-stream");
const { loggerEnv } = require("../Environment/config");
const fs = require("fs");
const DPAStream = require("./bunyan-dpa-stream");

// create dir path if not exists
fs.mkdirSync(loggerEnv.filePathDir, { recursive: true });

module.exports = ({ name, logLevel }) => {

  const logger = bunyan.createLogger({
    name: name || loggerEnv.appName,
    serializers: bunyan.stdSerializers,
    streams: [
      {
        name: "stdout",
        stream: process.stdout,
        level: logLevel && logLevel["stdout"] ? logLevel["stdout"] : loggerEnv.level
      },
      {
        name: "raw",
        stream: new DPAStream(),
        level: logLevel && logLevel["dpaStream"] ? logLevel["dpaStream"] : loggerEnv.level
      },
      {
        level: logLevel && logLevel["fileStream"] ? logLevel["fileStream"] : loggerEnv.level,
        type: "raw",
        stream: new RotatingFileStream({
          path: `${loggerEnv.filePathDir}/${loggerEnv.appName}-%Y-%m-%d-%H-%M-%S`,
          period: loggerEnv.period,
          totalFiles: loggerEnv.totalFiles,
          threshold: loggerEnv.threshold,
          totalSize: loggerEnv.totalSize,
          gzip: loggerEnv.gzip,
        }),
      },
    ],
  });

  const log_levels = ["fatal", "error", "warn", "info", "debug", "trace"];

  const logLevelWrappers = {};
  log_levels.forEach((level) => {
    logLevelWrappers[level] = (rrId, source, message, optionalParam) => {
      logger[level]({
        rrId, source, message, ...optionalParam, logLevel: level, app: loggerEnv.appName,
        timestamp: new Date()
      });
    }
  });
  return logLevelWrappers;
}
