# rr-js

### Installation

#### Install rr-js using npm

npm i git+http://repo.dfoundry.io/scm/rr/rr-js.git

#### Install peer dependecies

There are 2 libraries, one for nodejs and another for browser(which includes react, angular and etc.)

    For Browser:-
        @browser-bunyan/console-formatted-stream
        @browser-bunyan/server-stream
        browser-bunyan,
        axios,
        validator
    For NodeJS:-
        bunyan,
        bunyan-rotating-file-stream,
        validator,
        axios

#####

### Usage:-

#### Create a logger instance using either nodeBunyan or browserBunyan based on your environment

    const logger = require("./lib/nodeBunyan")({
        name: "test-service",
        logLevel: {
            stdout: "debug", fileStream: "debug", dpaStream: "debug"
        }
    });

- global log level can be set using an environment variable called LEVEL and then remove logLevel param from
  nodeBunyan or browserBunyan constructor

- Both browser and node version exposes below functions

  1. fatal
  2. error
  3. warn
  4. info
  5. debug
  6. trace

- All these take same arguments (rrId, source, message, optionalParam)

- In NodeJS

```
const logger = require("rr-js/lib/nodeBunyan")({name: "test-service"});
logger.info("11111", "cvcm", "some info log", {extra: info, key1: val1})
```

- In React or Angular

```
import browserBunyan from "rr-js/lib/browserBunyan.js";
const logger = browserBunyan({name: "test-service});
logger.info("11111", "cvcm", "some info log", {extra: info, key1: val1})
```

### Sample output

```
{
    "name":"test-service",
    "hostname":"DESKTOP-1LN919R",
    "pid":11096,
    "level":30,
    "rrId":"f59398ed-c03e-4ad6-8199-e3c2f988fb06",
    "source":"scheduler-service",
    "message":"info",
    "test":"test",
    "logLevel":"info",
    "app":"rr-js",
    "timestamp":"2021-03-23T16:11:40.504Z",
    "msg":"",
    "time":"2021-03-23T16:11:40.505Z",
    "v":0
}
```
